// Type definitions for socket.io
// Project: http://socket.io/
// Definitions by: William Orr <https://github.com/worr>
// Definitions: https://github.com/borisyankov/DefinitelyTyped


///<reference path='../node/node.d.ts' />

declare module "socket.io" {
	import http = require('http');
  
  function socketio(server: http.Server): socketio.Socket;

  module socketio {
    
    interface SocketHandler{
        (data: any): void;
    }

    interface Socket {
      on(eventName: string, handler: SocketHandler): void;
      emit(eventName: string, data: any): void;
      context: any;
    }

  }

  export = socketio;
}

