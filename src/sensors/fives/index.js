var cron= require('cron');
var argv = require('yargs').argv;
var client = require("socket.io-client");

var host = argv.host || "http://sensas-dispatcher.herokuapp.com/";
var socket = client.connect(host);

function emitAirHorn() {
    socket.emit('readSignal', { name: 'airhorn'}, function() {
      console.log("singal emitted");
    });
}

new cron.CronJob('0 55 8-18 * * 1-5', function(){
  console.log(Date.now(), 'Time for break!');
  if (socket.connected) {
	    emitAirHorn();
  } else {
	  socket.on('connect', emitAirHorn);
  }
}, null, true);
