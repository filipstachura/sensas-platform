var monitorOrientation = function(emitter, subscriber) {

  var ORIENTATION_TO_VALUE = {
    "landscape-primary": "1",
    "landscape-secondary": "1",
    "portrait-primary": "0"
  };
  var VALUE_2_ORIENTATION = {
    "0": "portrait",
    "1": "landscape"
  };
  var options = { frequency: 500 };
  var speed = {};
  var heading = undefined;

  function onSuccess(acceleration) {
    var noise = [1,55,10].map(function(elem) {
      return (Math.random() - 0.5) * elem;
    });
    var signals = {
      "acc_x": acceleration.x,
      "acc_y": acceleration.y,
      "acc_z": acceleration.z,
      "gyr_x": speed.x,
      "gyr_y": speed.y,
      "gyr_z": speed.z,
      "heading": heading,
      "noise1": noise[0],
      "noise2": noise[1],
      "noise3": noise[2],
      "orientation": ORIENTATION_TO_VALUE[screen.orientation]
    };
    speed = {};
    emitter("readSignal", {
      name: "acc", 
      value: signals,
      time: acceleration.timestamp
    });
  };

  function onError(error) {
    console.error(error);
  };

  navigator.accelerometer.watchAcceleration(onSuccess, onError, options);

  function onSuccessG(s) {
      speed.x = s.x;
      speed.y = s.y;
      speed.z = s.z;
  };

  function onErrorG(err) {
      console.error('onError!',err);
      delete speed.x;
      delete speed.y;
      delete speed.z;
  };

  function compassSuccess(h) {
      heading = h.magneticHeading;
  };
  function compassError(err) {
      console.error('onError!',err);
      heading = undefined;
  };

  navigator.gyroscope.watchAngularSpeed(onSuccessG, onErrorG,options);
  navigator.compass.watchHeading(compassSuccess, compassError, options);

  function setLearning(val) {
    $("#orientation-tab .start-learning").prop("disabled", val);
    $("#orientation-tab .stop-learning").prop("disabled", !val);
  }
  $("#orientation-tab .start-learning").click(function () {
    emitter("readSignal", {
      name: "start-learn-orientation", 
      value: undefined,
      time: Date.now() 
    });
    setLearning(true);
  });
  $("#orientation-tab .stop-learning").click(function () {
    emitter("readSignal", {
      name: "stop-learn-orientation", 
      value: undefined,
      time: Date.now() 
    });
    setLearning(false);
  });
  
  subscriber("prediction-orientation", function(signal) {
    var prediction = VALUE_2_ORIENTATION[signal.value];
    $("#orientationPrediction").val(prediction);
    setLearning(signal.predictorStatus);
  });
};

