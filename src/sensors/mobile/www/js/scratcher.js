function initScratcher(emitter) {
  $("#scratcher-tab button").click(function() {
    console.log(this.id);
    emitter("readSignal", {
      name: this.id, 
      value: undefined,
      time: Date.now() + config.serverTimeShift
    });
  });
}
