import client = require("socket.io-client");
import nodeBattery = require("node-battery");
import yargs = require('yargs');

var argv = yargs.argv;

var host = argv.host || "http://sensas-dispatcher.herokuapp.com/";
var socket = client.connect(host);
var ONE_SECOND = 1000;

var reportBattery = function reportBattery() {
  var batteryId = 0;
  nodeBattery.percentages(function(data){
    socket.emit("readSignal", {name: "ghost-battery", value: data[batteryId]});
  }, batteryId);
};

var interval = setInterval(reportBattery, ONE_SECOND);

process.on( 'SIGINT', function() {
  console.log("\nGracefully shutting down from SIGINT (Ctrl-C)");

  socket.close();
  clearInterval(interval);
  process.exit();
})
