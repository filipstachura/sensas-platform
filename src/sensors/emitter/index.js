var argv = require('yargs').argv;
var client = require("socket.io-client");

var host = argv.host || "http://sensas-dispatcher.herokuapp.com/";


function emit(signalName) {
  var socket = client.connect(host);
  socket.on('connect', function(){
    console.log('connection established');
    socket.emit('readSignal', { name: signalName }, function() {
      console.log("callback");
      socket.disconnect();
    });
  });
  socket.on('disconnect', function() {
    console.log("done. exiting...");
    process.exit();
  });
}

if (argv.emit) {
  emit(argv.emit);
}

