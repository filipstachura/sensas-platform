import client = require("socket.io-client");
import _ = require("lodash");
import fs = require("fs");
import MongoClient = require('mongodb').MongoClient;
import Q = require("q");
import yargs = require('yargs');

var argv = yargs.argv;

var host = argv.host || "http://sensas-dispatcher.herokuapp.com/";
var dbHost = argv.mogno || "mongodb://localhost:27017/sensas";
var socket;
var db;
var configPath = argv.config;
var start = argv.start;
var ONE_SECOND = 1000;
var shift = Date.now() - start;

if (!configPath) {
  console.log("Config file required. Please specify via --config parameter.");
  process.exit();
}

if (!start) {
  console.log("Start point in time required (epoch in miliseconds). Please specify via --start parameter.");
  process.exit();
}

var acceptSignals = fs.readFileSync(configPath).toString().split("\n");

function reportFromMongo(callNumber) {
  var min = start + ONE_SECOND * callNumber;
  var max = start + ONE_SECOND * (1+callNumber);
  var signals = db.collection('signals');

  console.log(min, max);
  var query = {
    name: {
      $in: acceptSignals
    },
    time: {
      $gte: min,
      $lt: max
    }
  }
  Q.npost(signals, "find", [query])
    .then(function(iterator) {
      return Q.npost(iterator, "toArray");
    })
    .then(function(signals) {
      _.forEach(signals, function(signal) {
        signal.time += shift;
        socket.emit("readSignal", signal);
        console.log("emitted", signal);
      });
    })
    .fail(function(err) {
      console.log("insert error: ", err);
    });
}

function connected(dbConnection) {
  console.log("Connected with mongo: ", dbHost);
  db = dbConnection;

  console.log("Connecting with socket.io:  ", host);
  socket = client.connect(host);
  socket.on('connect', function () {
    console.log("Connected with socket.io: ", host);
  });

  var callCounter = 0;
  setInterval(function() {
    reportFromMongo(callCounter);
    callCounter = callCounter + 1;
  }, ONE_SECOND);
}

function connectionFailed(err) {
  console.log("Not connected to mongo", err);
  process.exit();
}

function shutdown() {
  console.log("\nGracefully shutting down from SIGINT (Ctrl-C)");

  if (socket) {
    socket.close();
  }
  if (db) {
    db.close();
  }
  process.exit();
}

console.log("Connecting with mongo: ", dbHost);
Q.npost(MongoClient, "connect", [dbHost])
  .then(connected, connectionFailed);

process.on('SIGINT', shutdown);
