import Q = require("q");

interface IClassifier {
  getName(): string;
  getAttributes(): Array<string>;
  duplicate(withAttributes: Array<string>): IClassifier;
  learn(data: {[name: string]: any}): Q.Promise<void>;
  predict(data: {[name: string]: any}): Q.Promise<any>;
}

export = IClassifier;
