interface IEvaluator {
  name: string;
  report(predicted: any, expected: any): void;
  status(): number;
}

export = IEvaluator;
