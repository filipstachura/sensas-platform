import client = require("socket.io-client");
import jsyaml = require("js-yaml");
import _ = require("lodash");
import Q = require("q");
import yargs = require("yargs");
import fs = require("fs");
import csv = require('csv');
import path = require('path');

var argv = yargs.argv;
var host = argv.host || "http://sensas-dispatcher.herokuapp.com/";
var socket;
var configPath = argv.config;

var attrs;
var subscribe;
var className;
var headers;
var outputPath;

if (!configPath) {
  console.log("Config file required. Please specify via --config parameter.");
  process.exit();
}

function writeLine(line: string) {
  fs.appendFile(outputPath, line + "\n", function (err) {
    if (err) {
      console.error("Write line error", err);
    }
  });
}

Q(jsyaml.safeLoad(fs.readFileSync(configPath, "utf8")))
  .then((yaml: any): void => {
      attrs = yaml.attributes;
      subscribe = yaml.subscribe;
      className = yaml.class["name"];
      outputPath = path.join("./", className + "_" + (new Date().toISOString().replace(/T/, '_').replace(/\..+/, '')) + ".csv");

      console.log("Connecting with socket.io:  ", host);
      socket = client.connect(host);
      socket.on('connect', function () {
        writeLine(attrs.join(",")+","+className);
        socket.on(subscribe, reportSignal);
      });
  });

var reportSignal = function (signal) {
  writeLine(_.map(_.values(signal.value),(v) => Number(v).toFixed(2) ).join(","));
}

function shutdown() {
  console.log("\nGracefully shutting down.");
  if (socket) { socket.close(); }
  process.exit();
}
process.on('SIGINT', shutdown);
