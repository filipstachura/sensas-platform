import player = require('play-sound')(opts = {})
import client = require("socket.io-client");
import path = require('path');
import yargs = require('yargs');

var argv = yargs.argv;

var host = argv.host || "http://sensas-dispatcher.herokuapp.com/";
var socket = client.connect(host);

var sounds = {
  correct: path.join(__dirname, './sounds/correct.mp3'),
  wrong: path.join(__dirname, './sounds/wrong.mp3'),
  airhorn: path.join(__dirname, './sounds/airhorn.mp3')
};

socket.on('wrong', function() {
  console.log("wrong");
  player.play(sounds.wrong);
});

socket.on('airhorn', function() {
  console.log(Date.now(), "airhorn");
  player.play(sounds.airhorn);
});

socket.on('correct', function() {
  console.log("correct");
  player.play(sounds.correct);
});

process.on('SIGINT', function() {
  console.log("\nGracefully shutting down from SIGINT (Ctrl-C)");

  socket.close();
  process.exit();
})
