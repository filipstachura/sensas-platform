interface ISensasEvent {
  name: string;
  value: any;
  time: number;
};

export = ISensasEvent;
